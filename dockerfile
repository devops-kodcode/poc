from ubuntu:22.04

run apt update && \
    apt upgrade -y && \
    apt install git -y && \
    apt install wget -y && \
    apt install curl -y && \
    apt install python3 -y

run ln -s /usr/bin/python3 /usr/bin/python

add https://github.com/mikefarah/yq/releases/download/v4.40.5/yq_linux_amd64 \
    /usr/local/bin/yq

run chmod +x /usr/local/bin/yq

add https://gitlab.com/api/v4/projects/gitlab-org%2Frelease-cli/packages/generic/release-cli/latest/release-cli-linux-amd64 \
    /usr/local/bin/release-cli

run chmod +x /usr/local/bin/release-cli

CMD ["/bin/bash"]