const { writeFileSync } = require('fs')

const microServices = ["frontend", "products", "shoppingcart"]

const createJob = microService => `
build:${microService}:
  stage: build
  image: $CI_REGISTRY_IMAGE/docker.24.0.5-yq:1.0.0
  services:
    - docker:24.0.5-dind
  variables:
    DOCKER_HOST: tcp://docker:2376
    DOCKER_TLS_CERTDIR: "/certs"
    DOCKER_TLS_VERIFY: 1
    DOCKER_CERT_PATH: "$DOCKER_TLS_CERTDIR/client"
  before_script:
    - cd ${microService}
    - PREV_VERSION=$(yq -oy '.version' ./package.json)
    - NEW_VERSION=$(echo "$PREV_VERSION" | awk -F. '{print $1 "." $2 "." $3+1}')
    - yq -oj ".version = \\"$NEW_VERSION\\"" package.json -i
    - export IMAGE_NAME=$CI_REGISTRY_IMAGE/${microService}
    - export IMAGE_TAG=$NEW_VERSION
  script:
    - echo "$CI_REGISTRY_PASSWORD" | docker login -u "$CI_REGISTRY_USER" --password-stdin "$CI_REGISTRY"
    - docker build -t $IMAGE_NAME:$IMAGE_TAG .
    - docker push $IMAGE_NAME:$IMAGE_TAG
  rules:
    - if: $CI_COMMIT_BRANCH == "elad"
      changes:
        - "${microService}/**/*" 

`

const createDynamicGitLabFile = () => {
  // create content of ci file
  const fileContent = microServices.map(microService => createJob(microService))
                                   .reduce((job1, job2)=>{return job1 + job2})

  console.log(fileContent)

  // write file to disc
  writeFileSync('dynamic-build.gitlab-ci.yml', fileContent)
}

createDynamicGitLabFile()
