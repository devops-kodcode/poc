from docker:24.0.5

run apk update && \
    apk upgrade

add https://github.com/mikefarah/yq/releases/download/v4.40.5/yq_linux_amd64 \
    /usr/local/bin/yq

run chmod +x /usr/local/bin/yq

volume [/var/lib/docker]

entrypoint ["dockerd-entrypoint.sh"]

cmd []
